package edu.avans.library.presentation;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import edu.avans.library.businesslogic.ItemManager;
import edu.avans.library.domain.AbstractItem;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

public class ItemAdminUI extends JFrame {
	// Attributes
	private JComboBox comboBox;
	private JTextField textField;
	private ItemManager itemManager;
	private JButton editItemButton;
	private JButton removeItemButton;

	private String currentSearchType = null;
	private JTextPane itemTextPane;
	private JPanel itemBodyPanel;

	// Constructor
	public ItemAdminUI(ItemManager itemManager) {
		this.itemManager = itemManager;
		getContentPane().setLayout(new BorderLayout(0, 0));

		JLabel lblZoekItem = new JLabel("Zoek bestaande item");
		getContentPane().add(lblZoekItem, BorderLayout.NORTH);
		lblZoekItem.setMaximumSize(new Dimension(40, 14));
		lblZoekItem.setPreferredSize(new Dimension(40, 14));

		JPanel itemHomePanel = new JPanel();
		getContentPane().add(itemHomePanel, BorderLayout.CENTER);
		itemHomePanel.setLayout(new BorderLayout(0, 0));

		itemBodyPanel = new JPanel();
		itemHomePanel.add(itemBodyPanel, BorderLayout.CENTER);
		itemBodyPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
				JLabel lblId = new JLabel("ID:");
				itemBodyPanel.add(lblId);

		textField = new JTextField();
		itemBodyPanel.add(textField);
		textField.setColumns(10);
		
				JLabel lblType = new JLabel("Type:");
				itemBodyPanel.add(lblType);

		comboBox = new JComboBox();
		itemBodyPanel.add(comboBox);
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "boek", "dvd", "cd" }));

		JButton searchButton = new JButton("Zoeken");
		itemBodyPanel.add(searchButton);
		searchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Get currentSearchType from combobox and check what kind of
				// item it is
				String type = comboBox.getSelectedItem().toString();
				switch (type) {
				case "cd":
					currentSearchType = "cd";
					break;
				case "dvd":
					currentSearchType = "dvd";
					break;
				case "boek":
					currentSearchType = "book";
					break;
				}
				try {
					AbstractItem item = itemManager.findItem(currentSearchType, textField.getText().toString());
					// add item into textpane
					setItemTextPane(item);

				} catch (NullPointerException npe) {
					JOptionPane.showMessageDialog(null, "Item kan niet worden gevonden in de database!", "Foutmelding",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JPanel itemNavigationPanel = new JPanel();
		itemHomePanel.add(itemNavigationPanel, BorderLayout.SOUTH);

		removeItemButton = new JButton("Verwijderen");
		itemNavigationPanel.add(removeItemButton);
		removeItemButton.setEnabled(false);

		editItemButton = new JButton("Bewerken");
		itemNavigationPanel.add(editItemButton);
		editItemButton.setEnabled(false);
		
		itemTextPane = new JTextPane();

	}

	public void setItemTextPane(AbstractItem item) {
		itemTextPane.revalidate();
		itemTextPane.setBounds(10, 109, 364, 142);
		itemBodyPanel.add(itemTextPane);
		itemTextPane.setText(item.toString());
		itemTextPane.setEditable(false);

		// enable edit and delete buttons

		editItemButton.setEnabled(true);
		removeItemButton.setEnabled(true);
	}
}
