package edu.avans.library.presentation;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import edu.avans.library.businesslogic.ItemManager;
import edu.avans.library.businesslogic.MemberAdminManagerImpl;

public class MainFrame extends JFrame {
	// Attributes
	private MemberAdminManagerImpl memberManager;
	private JPanel mainPanel;
	
	// Constructor
	public MainFrame(MemberAdminManagerImpl memberManager) {
		setTitle("Bibliotheek Applicatie");
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		mainPanel = new JPanel();
		getContentPane().add(mainPanel, BorderLayout.CENTER);
		mainPanel.setLayout(new BorderLayout(0, 0));
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JButton memberPageButton = new JButton("Leden beheren");
		//memberPageButton.addActionListener(new memberPageHandler());
		memberPageButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MemberAdminUI ui = new MemberAdminUI(new MemberAdminManagerImpl());
				mainPanel.removeAll();
				mainPanel.add(ui.getContentPane());
				mainPanel.revalidate();
			}
		});
		menuBar.add(memberPageButton);
		
		JButton itemPageButton = new JButton("Item beheren");
		itemPageButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ItemAdminUI itemUI = new ItemAdminUI(new ItemManager());
				mainPanel.removeAll();
				mainPanel.add(itemUI.getContentPane());
				mainPanel.revalidate();
			}
		});
		menuBar.add(itemPageButton);
		
		JButton addItemButton = new JButton("Item toevoegen");
		addItemButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddNewItemUI newItemUI = new AddNewItemUI(new ItemManager());
				mainPanel.removeAll();
				mainPanel.add(newItemUI);
				mainPanel.revalidate();
			}
		});
		menuBar.add(addItemButton);
		
		// Setup Frame
		setSize(400,400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
