package edu.avans.library.presentation;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import edu.avans.library.businesslogic.ItemManager;
import edu.avans.library.domain.AbstractItem;
import edu.avans.library.domain.Book;
import edu.avans.library.domain.CD;
import edu.avans.library.domain.DVD;

public class AddNewItemUI extends Panel {
	// Attributes
	private JTextField itemIDField;
	private JTextField itemTitleField;
	private JTextField itemAuthorField;
	private JTextField itemEditionField;
	private JTextField itemISBNField;
	private JTextField itemAmountOfSongsField;
	private JTextField itemDurationField;
	private JTextField itemGenreField;

	//
	// labels
	private JLabel idLabel;
	private JLabel titleLabel;
	private JLabel authorLabel;
	private JLabel editionLabel;
	private JLabel isbnLabel;
	private JLabel songsLabel;
	private JLabel mdurationLabel;
	private JLabel genreLabel;

	// Button
	private JButton saveItemButton;
	private JPanel bodyPanel;
	private JPanel headerPanel;
	private JLabel selectTypeLabel;
	private JComboBox<String> typeComboBox;

	private String itemType;
	private AbstractItem newItem;
	
	// Manager
	private ItemManager itemManger;

	// Constructor
	public AddNewItemUI(ItemManager itemManger) {
		this.itemManger = itemManger;
		
		setLayout(new BorderLayout(0, 0));

		bodyPanel = new JPanel();
		// add(bodyPanel);
		bodyPanel.setLayout(new GridLayout(0, 2, 0, 0));

		idLabel = new JLabel("Item ID:");
		bodyPanel.add(idLabel);

		// initialize objects
		itemIDField = new JTextField(10);
		bodyPanel.add(itemIDField);
		titleLabel = new JLabel("Titel:");
		bodyPanel.add(titleLabel);
		itemTitleField = new JTextField(10);
		bodyPanel.add(itemTitleField);
		authorLabel = new JLabel("Auteur");
		bodyPanel.add(authorLabel);
		itemAuthorField = new JTextField(10);
		bodyPanel.add(itemAuthorField);
		editionLabel = new JLabel("Editie:");
		bodyPanel.add(editionLabel);
		itemEditionField = new JTextField(10);
		bodyPanel.add(itemEditionField);
		isbnLabel = new JLabel("ISBN");
		bodyPanel.add(isbnLabel);
		itemISBNField = new JTextField(10);
		bodyPanel.add(itemISBNField);
		songsLabel = new JLabel("Liedjes:");
		bodyPanel.add(songsLabel);
		itemAmountOfSongsField = new JTextField(10);
		bodyPanel.add(itemAmountOfSongsField);
		mdurationLabel = new JLabel("Film tijd:");
		bodyPanel.add(mdurationLabel);
		itemDurationField = new JTextField(10);
		bodyPanel.add(itemDurationField);
		genreLabel = new JLabel("Genre:");
		bodyPanel.add(genreLabel);
		itemGenreField = new JTextField(10);
		bodyPanel.add(itemGenreField);

		JLabel label = new JLabel("");
		bodyPanel.add(label);

		saveItemButton = new JButton("OPSLAAN");
		bodyPanel.add(saveItemButton);

		headerPanel = new JPanel();
		add(headerPanel, BorderLayout.NORTH);

		selectTypeLabel = new JLabel("Selecteer itemtype:");
		headerPanel.add(selectTypeLabel);

		typeComboBox = new JComboBox<String>();
		typeComboBox.setModel(new DefaultComboBoxModel(new String[] { "boek", "dvd", "cd" }));
		headerPanel.add(typeComboBox);

		// // Listeners
		typeComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Get item type from combobox
				itemType = typeComboBox.getSelectedItem().toString();
				add(bodyPanel);
				switch (itemType) {
				case "boek":
					itemAmountOfSongsField.setEnabled(false);
					itemDurationField.setEnabled(false);
					itemGenreField.setEnabled(false);
					bodyPanel.revalidate();
					bodyPanel.repaint();
					break;
				case "dvd":
					itemISBNField.setEnabled(false);
					itemAmountOfSongsField.setEnabled(false);
					bodyPanel.revalidate();
					bodyPanel.repaint();
					break;
				case "cd":
					itemISBNField.setEnabled(false);
					itemDurationField.setEnabled(false);
					itemGenreField.setEnabled(false);
					bodyPanel.revalidate();
					bodyPanel.repaint();
					break;
				}			

			}
		});

		// Listener save item
		saveItemButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch (itemType) {
				case "boek":
					newItem = new Book(itemIDField.getText(), itemTitleField.getText(), itemAuthorField.getText(),
							itemEditionField.getText(), Integer.parseInt(itemISBNField.getText()));
					itemManger.addItem(itemType, newItem);
					break;

				case "dvd":
					newItem = new DVD(itemIDField.getText(), itemTitleField.getText(), itemAuthorField.getText(),
							itemEditionField.getText(), Integer.parseInt(itemDurationField.getText()),
							itemGenreField.getText());
					itemManger.addItem(itemType, newItem);
					break;

				case "cd":
					newItem = new CD(itemIDField.getText(), itemTitleField.getText(), itemAuthorField.getText(),
							itemEditionField.getText(), Integer.parseInt(itemAmountOfSongsField.getText()));
					itemManger.addItem(itemType, newItem);
					break;
				}
				if(newItem != null) {
					bodyPanel.removeAll();
					JTextArea textArea = new JTextArea();
					textArea.append(newItem.toString());
					bodyPanel.add(textArea);
					bodyPanel.revalidate();
					bodyPanel.repaint();
				}
			}
		});

	}
}
