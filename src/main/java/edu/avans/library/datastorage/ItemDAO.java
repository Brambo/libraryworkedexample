package edu.avans.library.datastorage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLException;

import edu.avans.library.domain.AbstractItem;
import edu.avans.library.domain.CD;
import edu.avans.library.domain.DVD;

public class ItemDAO {
	// Attributes
	private AbstractItem item = null;
	private String getItemID;
	private String itemTitle;
	private String itemAuthor;
	private String itemEdition;
	
	private String secondSubQuery;

	public AbstractItem findItem(String itemType, String itemID) {

		// First open a database connnection
		DatabaseConnection connection = new DatabaseConnection();
		if (connection.openConnection()) {
			// If a connection was successfully setup, execute the SELECT
			// statement.
			String query = "SELECT * FROM item NATURAL JOIN " + itemType + " WHERE itemID  = '" + itemID + "';";
			ResultSet resultset = connection.executeSQLSelectStatement(query);
			if (resultset != null) {
				try {
					while (resultset.next()) {
						getItemID = resultset.getString("itemID");
						itemTitle = resultset.getString("title");
						itemAuthor = resultset.getString("author");
						itemEdition = resultset.getString("edition");

						switch(itemType) {
						case "book":
							int isbn = resultset.getInt("ISBN");
							item = new CD(getItemID, itemTitle, itemAuthor, itemEdition, isbn);
							break;
						case "cd":
							int amountOfSongs = resultset.getInt("amountOfSongs");
							item = new CD(getItemID, itemTitle, itemAuthor, itemEdition, amountOfSongs);
							break;
						case "dvd":
							int movieDuration = resultset.getInt("movieDuration");
							String genre = resultset.getString("genre");
							item = new DVD(getItemID, itemTitle, itemAuthor, itemEdition, movieDuration, genre);
							break;
							
					}

					}
				} catch (SQLException e) {
					System.out.println(e);
				}
			}

			// We had a database connection opened. Since we're finished,
			// we need to close it.
			connection.closeConnection();
		}

		return item;
	}

//	public AbstractItem editItem(String itemType, String itemID) {
//		// First open a database connnection
//		DatabaseConnection connection = new DatabaseConnection();
//		if (connection.openConnection()) {
//			String query = "SELECT * FROM item NATURAL JOIN " + itemType + " WHERE itemID  = '" + itemID + "';";
//			ResultSet resultset = connection.executeSQLSelectStatement(query);
//			System.out.println(query);
//			System.out.println(resultset.toString());
//		}
//	}
	
	public void addItem(String itemType, AbstractItem item) {
		
		 // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if(connection.openConnection()) {
        	
        	 // If a connection was successfully setup query
        	
        	//Main query for all items
        	 String query = ("INSERT INTO `library`.`item` "
             		+ "(`itemID`, `title`, `Author`, `Edition`) "
             		+ "VALUES ('" + item.getItemID() + "','" + item.getTitle() + "','" + item.getAuthor() + "','" + item.getEdition() +"');");
        	
        	 // sub query for specific item
        	if(itemType.equals("cd")) {
        		CD book = (CD) item;
        		secondSubQuery = "INSERT INTO `library`.`cd` (`amountOfSongs`, `ItemID`) "
        				+ "VALUES ('" + book.getAmountOfSongs() + "','" + book.getItemID() +"');";
        	}
           
            // Add query into database
            connection.executeUpdateStatement(query);
            connection.executeUpdateStatement(secondSubQuery);
            System.out.println("Query test:");
            System.out.println(query);
            System.out.println(secondSubQuery);
            
            // Close the connection
            connection.closeConnection();
        }
		
	}
	
	public void removeItem(String itemID) {
		
		DatabaseConnection connection = new DatabaseConnection();
		if(connection.openConnection()) {
			
			String query = "DELETE FROM ITEM WHERE ITEMID = '" + itemID + "';";
			System.out.println(query);
			connection.executeSQLDeleteStatement(query);
			System.out.println("Item deleted");
        }
        connection.closeConnection();
    }
}
