package edu.avans.library.businesslogic;

import java.util.HashMap;
import java.util.Map;

import edu.avans.library.datastorage.ItemDAO;
import edu.avans.library.domain.AbstractItem;
import edu.avans.library.domain.Book;
import edu.avans.library.domain.DVD;

public class ItemManager {
	// Attributes
	private Map<String, AbstractItem> items;

	// Constructor
	public ItemManager() {
		items = new HashMap<>();

	}


	// Methods
	public AbstractItem addItem(String type, AbstractItem item) {
		ItemDAO itemDAO = new ItemDAO();
		itemDAO.addItem(type, item);
		return item;
			
		}


	public AbstractItem findItem(String itemType, String itemID) {
		ItemDAO itemDAO = new ItemDAO();

		AbstractItem item = itemDAO.findItem(itemType, itemID);
		return item;
	}
	
	public void removeItem(String itemID) {
		ItemDAO itemDAO = new ItemDAO();
		itemDAO.removeItem(itemID);
	}
}
