package edu.avans.library.domain;

import java.util.ArrayList;
import java.util.List;

public class AbstractItem {
	// Attributes
	protected String itemID;
	protected String title;
	protected String author;
	protected String edition;
	
	// List
    private final List<Copy> copies;
    private final List<Reservation> reservations;

	// Constructor
	public AbstractItem(String itemID, String title, String author, String edition) {
		this.itemID = itemID;
		this.title = title;
		this.author = author;
		this.edition = edition;
		// initialize lists
        copies = new ArrayList<>();
        reservations = new ArrayList<>();
	}

	// Methods
	
    public void removeReservation(Reservation reservation) {
        reservations.remove(reservation);
    }
    
    
	public String getItemID() {
		return itemID;
	}

	public void setItemID(String itemID) {
		this.itemID = itemID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Het volgende item is gevonden in de database:\n");
		sb.append("ItemID:\t ").append(itemID);
		sb.append("\nTitel:\t ").append(title);
		sb.append("\nAuteur:\t ").append(author);
		sb.append("\nEditie:\t ").append(edition);
		return sb.toString();
	}
}
