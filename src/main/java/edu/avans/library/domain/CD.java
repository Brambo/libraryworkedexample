package edu.avans.library.domain;



public class CD extends AbstractItem {
	// Attributes
	private int amountOfSongs;

	public CD(String itemID, String title, String author, String edition, int amountOfSongs) {
		super(itemID, title, author, edition);
		this.amountOfSongs = amountOfSongs;
	}

	public int getAmountOfSongs() {
		return amountOfSongs;
	}

	public void setAmountOfSongs(int amountOfSongs) {
		this.amountOfSongs = amountOfSongs;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Het volgende item is gevonden in de database:\n");
		sb.append("ItemID:\t ").append(itemID);
		sb.append("\nTitel:\t ").append(title);
		sb.append("\nAuteur:\t ").append(author);
		sb.append("\nEditie:\t ").append(edition);
		sb.append("\nLiedjes:\t ").append(amountOfSongs);
		return sb.toString();
	}

}
