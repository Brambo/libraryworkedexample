/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.avans.library.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ppthgast
 */
public class Book extends AbstractItem {
	
	// Attributes
	private final int isbn;

	// Constructor
    public Book(String itemID, String title, String author, String edition, int isbn) {
		super(itemID, title, author, edition);
		this.isbn = isbn;
	}

    // Methods
    public int getISBN() {
        return isbn;
    }

    @Override
    public boolean equals(Object o) {
        boolean equal = false;

        if (o == this) {
            // same instance of the class, so equal by definition
            equal = true;
        } else {
            if (o instanceof Book) {
                Book b = (Book) o;

                // Book is identified by its ISBN, so only checking ISBN is sufficient.
                equal = this.isbn == b.isbn;
            }
        }

        return equal;
    }

    @Override
    public int hashCode() {
        // This implementation is based on the best practice described 
        // in Effective Java, 2nd edition, Joshua Bloch.

        // ISBN is unique, so satisfying as hashCode.
        return isbn;
    }
    
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Het volgende item is gevonden in de database:\n");
		sb.append("ItemID:\t ").append(itemID);
		sb.append("\nTitel:\t ").append(title);
		sb.append("\nAuteur:\t ").append(author);
		sb.append("\nEditie:\t ").append(edition);
		sb.append("\nISBN:\t ").append(isbn);
		return sb.toString();
	}
}
