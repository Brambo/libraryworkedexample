package edu.avans.library.domain;

public class DVD extends AbstractItem {
	// Attributes
	private int movieDuration;
	private String genre;

	// Constructor
	public DVD(String itemID, String title, String author, String edition, int movieDuration, String genre) {
		super(itemID, title, author, edition);
		this.movieDuration = movieDuration;
		this.genre = genre;
	}

	// Methods
	public int getMovieDuration() {
		return movieDuration;
	}

	public void setMovieDuration(int movieDuration) {
		this.movieDuration = movieDuration;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Het volgende item is gevonden in de database:\n");
		sb.append("ItemID:\t ").append(itemID);
		sb.append("\nTitel:\t ").append(title);
		sb.append("\nAuteur:\t ").append(author);
		sb.append("\nEditie:\t ").append(edition);
		sb.append("\nDuur:\t ").append(movieDuration).append(" minuten");
		sb.append("\nGenre:\t ").append(genre);
		return sb.toString();
	}

}
